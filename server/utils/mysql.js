// 引入myspl模块
const mysql=require('mysql');

// 创建连接池
const pool=mysql.createPool({
    host:'localhost',
    user:'root',
    password:'mysql',
    database:'demo1',
    connectionLimit:'20',
    timezone:'utc',
});
// 导出操作数据库的标准方法
module.exports = function(sql,params){
    // 返回promise对象
    return new Promise((resolve,reject)=>{
    // 先从连接池中取一个链接
        pool.getConnection((err,poolconnection)=>{
            if(err){
                console.error(err);
                reject(err);
            }else{
                // 没有错误正常取得链接
                // 使用这个链接来操作数据库
                poolconnection.query(sql,params,(err,result)=>{
                    // 查询完成后，优先释放链接到链接池
                    poolconnection.release();
                    // 判断是否为真
                    if(err){
                        console.error(err);
                        reject(err);
                    }else{
                        // 没有错误，查询成功
                        resolve(result);
                    }                               
                })
            }
        })
    })



                                   
}