var express = require('express');
var router = express.Router();
const mysql = require('../utils/mysql.js');
const jiami = require('../utils/jiami.js');
const jwt = require('jsonwebtoken')
const secretKey = "abc"
const { expressjwt } = require('express-jwt');
let upload = require('../utils/upload.js')



/* GET home page. */
// router.get('/getdata',async function(req,res){
//     // res.json(req.query);
//     // 获取从客户端传入的用户名和密码
//     let {username,password} = req.query
//     // 将用户名和密码插入数据库中的users表
//     const result=await mysql(
//     'insert into users (username,password,createtime,updatatime) values (?,?,now(),now())',
//     [username,password]
//     )
//     console.log(result);
//     res.json({
//         flag:true,
//         msg:'用户注册成功',


//     })
// })
// router.all('/getdata',async function(req,res){
//     res.json({
//         query:req.query,
//         body:req.body,
//         params: req.params,
//     })
// })
// router.post('/getdata',async function(req,res){
//     res.json({
//         body:req.body,
//         params:req.params,
//         query:req.query
//     })
// })

/**
 * 用户注册
 */
router.post('/getdata', async function (req, res) {
    const { uid, pwd } = req.body;


    if (uid.length > 6 || pwd.length < 5) {
        res.json({
            flag: false,
            msg: '用户名密码格式不正确'
        })
    } else {
        const [{ count }] = await mysql('select count(*) as count from users where username = ?', [uid])
        if (count > 0) {
            res.json({
                flag: false,
                msg: "用户名重复"
            })
        } else {
            await mysql('insert into users (username,password,createtime,updatatime) values (?,?,?,?)', [uid, jiami(pwd), new Date(), new Date()])
            res.json({
                flag: true,
                msg: "成功"
            })
        }
    }
})
/**
 * 用户登录
 */
router.post('/userlogin', async function (req, res) {
    const { uid, pwd } = req.body;
    if (uid.length > 6 || pwd.length < 5) {
        res.json({
            flag: false,
            msg: "密码或用户名格式不正确"
        })
    } else {
        const result = await mysql('select * from users where username=? and password=?', [uid, jiami(pwd)]);
        if (result.length > 0) {
            res.json({
                flag: true,
                msg: '登录成功',
                data: {
                    token: jwt.sign({ username: uid, id: result[0].id }, secretKey, { expiresIn: 60 * 60 })
                }
            })
        } else {
            res.json({
                flag: false,
                msg: '密码错误'
            })
        }
    }
})
router.all('*', expressjwt({
    secret: secretKey,//密钥
    algorithms: ['HS256'] // t的加密算法
}))
// router.all('/home',function(res,req){
//     // res.json({
//     //     a:1
//     // })
// })
// 单个文件用single，多个用array，多个组件用fileds
// router.post('/upload', upload.fields([
//     {name:'img',maxCount:2},
//     {name:'pic',maxCount:2},
//     {name:"head"}
//   ]), function (req, res) {
//     res.json({
//       1:req.body,
//       2:req.files,
//     })
//   })
router.get("/usersinfo", async function (req, res, next) {
    const userid = req.auth.id;
    const user = await mysql('select * from usersinfo where userid = ? ', [userid])
    if (user.length > 0) {
        res.json({
            flag: true,
            data: user[0],
        })
    } else {
        const newUser = await mysql('insert into usersinfo (createtime,updatatime,userid) values (now(),now(),?)', [userid])
        res.json({
            flag: true,
            data: {},
        })
    }
})
router.post("/updateuserinfo", upload.single("avatar"), async function (req, res) {
    let { nickname, gender, birthday, avatar } = req.body;
    const userid = req.auth.id;

    let avatar_filed = [];
    let avatar_arr = [];
    if (nickname) {
        avatar_filed.push("nickname=?")
        avatar_arr.push(nickname)
    }
    if (gender) {
        avatar_filed.push("gender=?")
        if (gender == "男") {
            avatar_arr.push(true)
        }
        if (gender == "女") {
            avatar_arr.push(false)
        }
    }
    if (birthday) {
        avatar_filed.push("birthday=?")
        avatar_arr.push(new Date(birthday))
    }
    if (avatar) {
        avatar_filed.push("avatar=?")
        avatar_arr.push(avatar)
    }
    avatar_arr.push(userid)
    let sql = '';
    sql = "UPDATE usersinfo SET ";
    sql += avatar_filed.join(",");
    sql += " where userid=?"

    const usersql = await mysql(sql, avatar_arr)
    // console.log(usersql);
    res.json({
        flag: true,
        msg: "设置成功"
    })
})
router.post("/addnote", async function (req, res) {
    const { title, content } = req.body;
    const userid = req.auth.id;
    await mysql('insert into notebook (title,content,userid) values (?,?,?)', [title, content, userid])
    res.json({
        flag: true,
        msg: "添加成功"
    })
})
router.get("/getnote", async function (req, res) {
    let { search_string, page, per_page } = req.query;
    const userid = req.auth.id;

    if (!search_string) {
        search_string = ''
    }
    page = Number(page)
    if (Number.isNaN(page)) {
        page = 1
    } else {
        if (page < 1) {
            page = 1
        }
    }
    per_page = Number(per_page)
    if ((Number.isNaN(per_page))) {
        per_page = 10
    }
    // else{
    //     if(per_page < 10){
    //         per_page = 10
    //     }
    // }
    per_page = Number(per_page)
    const result = await mysql('select * from notebook where title like ? and userid = ? order by updateTime desc limit ?,?', ['%' + search_string + '%', userid, (page - 1) * per_page, per_page])
    console.log(['%' + search_string + '%', userid, (page - 1) * per_page, per_page]);
    // const [{ count: total }] = await mysql('select count(*) as from notebook where title like ? and userid = ?', 
    // ['%' + search_string + '%', userid]
    // )
    const [{count:total}]=await mysql('select count(*) as count from notebook where title like ? and userid = ?',
    ['%' + search_string + '%', userid])
    res.json({

        flag: true,
        data: {
            list: result,
            total: total,
            pagesize: per_page,
        }

    })
})

module.exports = router;
