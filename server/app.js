var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('express-async-errors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);
app.use(function(err,req,res,next){
    if(err.name == "UnauthorizedError"){
        console.log(req.url);
        if(/^\/api\/usersinfo/.test(req.url)){
            res.json({
                flag:false,
                msg:'token失败'
            })
        }else{
            res.status(401).send("invalid token...");    
        }
    }else{
        next(err);
    }
})
module.exports = app;
