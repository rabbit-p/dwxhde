export default function(method,url,params={}){
    return new Promise(function(resolve,reject){
        const xhr=new XMLHttpRequest()
        xhr.onreadystatechange=function(){
            if(xhr.readyState==4){
                if(xhr.status==200){
                    resolve(JSON.parse(xhr.response) )
                }else{
                    if(xhr.status==401){
                        alert('登录超时或未登录')
                        location.replace('../login.html')

                    }else{
                        alert('错误')
                        reject(xhr.status)    
                    }

                }
            }
        }
        const data=Object.entries(params).map(item => item[0] + '=' + encodeURIComponent(item[1])).join('&');
        if(method.toLowerCase()=='get'){
            url +="?" + data
        }
        xhr.open(method,url,true)
        if(method.toLowerCase()=='post'){
            // xhr.setRequestHeader('Content-Type', 'Application/x-www-form-urlencoded');
            xhr.setRequestHeader('Content-Type', 'application/json');
        }
        const token=localStorage.getItem("token")
        xhr.setRequestHeader('Authorization','Bearer '+token);
        xhr.send(method.toLowerCase()=='get'?null:JSON.stringify(params))
    })
}