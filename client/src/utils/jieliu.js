
export default function (fn, delay) {
    let timer = null;
    return function () {
      if (timer) { return }
      fn.apply(this, arguments)//传递this指向
      timer = setTimeout(() => {
        timer = null;
      }, delay);
    }
  }