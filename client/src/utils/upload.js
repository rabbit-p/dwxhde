export default function(fd=new FormData(),url){
    return new Promise((resolve,reject)=>{
        const xhr= new XMLHttpRequest()
    xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
               
                resolve(JSON.parse(xhr.response))    
                
            }else{
                if(xhr.status==401){
                    alert('登录尝试或登录失败')
                    location.replace('../login.html')
                }
                else{
                    alert("error")
                    reject(xhr.status)
                }
            }
        }
    }
    xhr.upload.onprogress=function (e){
        if(e.lengthComputable){
            const p=e.loaded/ e.total *100 +'%'
            // bar.style.width=p
        }
    }
    xhr.open("post",url,true)
    const token=localStorage.getItem("token")
    xhr.setRequestHeader('Authorization','Bearer '+token);
    xhr.send(fd)
    })
}