
import ajax from '../../utils/ajx.js'

const form=document.forms.add;
console.log(form)
form.onsubmit=form_add;

function form_add(e){
    e.preventDefault();
    if((!form.title.value) || (!form.content.value)){
        alert("未填写")
        return
    }
    ajax("post","/api/addnote",{
        title:form.title.value,
        content:form.content.value,
    }).then(result=>{
        alert(result.msg);
        if(confirm("还要继续吗")){
            form.reset()
        }else{
            location('../../home.html')
        }
    })
}