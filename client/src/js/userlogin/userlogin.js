import ajax from '../../utils/ajx.js'
// import jieliu from '../utils/jieliu.js'



const form = document.forms.login

// form.onsubmit = sub;

form.onsubmit = function (e) {
  e.preventDefault()
  ajax('post', '/api/userlogin', {
    uid: form.uid.value,
    pwd: form.pwd.value,
  }).then(result => {
    if(result.flag){
      alert(result.msg)
      localStorage.setItem("token", result.data.token)
      location.replace('../../home.html')
    }
   else{
    alert(result.msg)
    form.uid.select()
   }

  })

}