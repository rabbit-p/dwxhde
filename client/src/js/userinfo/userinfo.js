import ajax from '../../utils/ajx.js'
// import laydate from 'laydate-es6'
import upload from '../../utils/upload.js'
const form = document.forms[0]
const img = document.querySelector("img")
const avatar = document.getElementById("avatar1")


init()
async function init() {

    form_reset();
    // laydateform();


}
// function laydateform() {
//     laydate.render({
//         elem: "#birthday",
//         max: '2023-1-5'
//     })
// }
async function form_reset() {
    const { data } = await ajax("get", "/api/usersinfo")
    console.log(data);
    try {
        if (data.nickname) {
            form.nickname.value = data.nickname
        }
        if (data.birthday) {
            form.birthday.value = data.birthday
        }
        if (data.gender) {
            form.gender.value = data.gender.data[0] == 0 ? "男" : "女";
        }
        if (data.avatar) {
            form.avatar.value = data.avatar
        }
    } catch (error) {

    }

}
form.onsubmit = form_sub;
avatar.onchange = avatar_change;

function avatar_change() {
    const that = this;
    // 图片预览
    if (that.files.length > 0) {
        if (!/^image\//.test(that.files[0].type)) {
            alert("请选择图片文件")
            that.value = null;
            img.src = '';
            return
        } if (that.files[0].size > 1024 * 500) {
            alert("图片过大")
            that.value = null;
            img.src = '';
            return
        }
        img.src = URL.createObjectURL(that.files[0])
    } else {
        img.src = '';
    }

}

function form_sub(e) {
    e.preventDefault()
    const fd = new FormData(form);
    upload(fd, '/api/updateuserinfo').then(function (result) {

    })

}