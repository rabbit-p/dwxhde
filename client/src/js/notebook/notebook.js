import ajax from '../../utils/ajx'

const datalist=document.getElementById("datalist")
const pageSelect=document.getElementById("pageSelect")
let per_page = 2;
let page = 1;
let search_string = '';
let pageTotal;


init();
function init(){
    showData()
}
function showData(){
    datalist.innerHTML=""
    ajax("get","/api/getnote",{
        search_string,
        page,
        per_page
    }).then(( {data:{list,total,pagesize}})=>{
        // console.log( {data:{list,total,pagesize}});
        // console.log(data)
        per_page=pagesize;
        pageTotal=Math.ceil(total/pagesize)
        console.log(pageTotal,pagesize,total)
        creatPageselect();
        list.forEach(item => {
            const tr=document.createElement("tr")
            let td;

            td=document.createElement("td")
            td.innerText = item.id;
            tr.appendChild(td);

            td=document.createElement("td")
            td.innerText = item.title;
            tr.appendChild(td);

            td=document.createElement("td")
            td.innerText = item.updateTime.slice(0,10);
            tr.appendChild(td);

            td=document.createElement("td")
            td.innerText = item.createTime.slice(0,10);
            tr.appendChild(td);

            td=document.createElement("td")
            const modifyBtn=document.createElement('button')
            modifyBtn.innerText="修改"
            modifyBtn.onclick=modifyBtn_click.bind(item.id)
            td.appendChild(modifyBtn)

            const delectBtn=document.createElement('button')
            delectBtn.innerText="删除"
            delectBtn.onclick=delectBtn_click.bind(item.id)
            td.appendChild(delectBtn)
            
            tr.appendChild(td)
            datalist.appendChild(tr)
        });
    })
}
function modifyBtn_click(){
    this; 
}

function delectBtn_click(){
    this; 
}
function creatPageselect(){
    // 首页
    pageSelect.innerHTML="";
    let btn
     btn=document.createElement("button")
    btn.innerText="首页"
    btn.onclick=function(){
        page=1;
        showData()
    }
    pageSelect.appendChild(btn)

    btn=document.createElement("button")
    btn.innerText="上一页"
    if(page > 1){
        btn.onclick=function(){
            page--;
            showData()
        }
    }
   
    pageSelect.appendChild(btn)
    
    let start=page-2>1?page-2:1;
    let end=page+2<pageTotal?page+2:pageTotal;
    for(let i=start;i<=end;i++){
        btn=document.createElement("button")
         btn.innerText=i
         if(page==i){
            btn.style.backgroundColor = 'yellow'
         }else{
            btn.onclick=function(){
                page = i;
                showData()
            }
         }
       
         pageSelect.appendChild(btn)
             
     }

    btn=document.createElement("button")
        btn.innerText="下一页"
        if(page<pageTotal){
            btn.onclick=function(){
                page++;
                showData()
            }
        }
        
        pageSelect.appendChild(btn)

   

   
    // if(page>1){
    //     btn=document.createElement("button")
    //     btn.innerText="首页"
    //     btn.onclick=function(){
    //         page=;
    //         showData()
    //     }
    //     pageSelect.appendChild(btn) 
    // }
    
    
    // 尾页
     btn=document.createElement("button")
     btn.innerText="尾页"
     btn.onclick=function(){
        page=pageTotal;
        showData()
    }
    pageSelect.appendChild(btn)



}