const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


module.exports = {
  //mode是打包模式：development|production|none
  mode: 'development',

  //定义入口
  entry: {
    index: './src/js/index.js',
    login:'./src/js/login.js',
    home:'./src/js/home.js',
    userinfo:'./src/js/userinfo.js',
    addnote:'./src/js/addnote.js'
  },

  //配置输出
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: 'js/[id]_bundle.js',//[id]、[name]、[hash]、[chunkhash]
    clean: true,//表示每次编译前，先清空输入文件夹
  },

  //配置webpack需要用到的插件
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',//模板文件地址
      filename: 'index.html',//生成后的文件名
      chunks: ['index'],//生成后的html文件自动引入哪些js文件
    }),

    new HtmlWebpackPlugin({
      template: './src/login.html',//模板文件地址
      filename: 'login.html',//生成后的文件名
      chunks: ['login'],//生成后的html文件自动引入哪些js文件
    }),
    new HtmlWebpackPlugin({
      template: './src/home.html',//模板文件地址
      filename: 'home.html',//生成后的文件名
      chunks: ['home'],//生成后的html文件自动引入哪些js文件
    }),
    new HtmlWebpackPlugin({
      template: './src/userinfo.html',//模板文件地址
      filename: 'userinfo.html',//生成后的文件名
      chunks: ['userinfo'],//生成后的html文件自动引入哪些js文件
    }),
    new HtmlWebpackPlugin({
      template: './src/addnote.html',//模板文件地址
      filename: 'addnote.html',//生成后的文件名
      chunks: ['addnote'],//生成后的html文件自动引入哪些js文件
    }),
    // MiniCssExtractPlugin
    new MiniCssExtractPlugin({
      //生成的文件名及路径
      filename: 'css/main_[id].css'
    }),

    //复制src源代码中的，不需要wenpack处理的资源到dist目录中
    // new CopyWebpackPlugin({
    //   patterns: [
    //     {
    //       from: './src/images',
    //       to: 'images'//本身就相对于dist目录
    //     },
    //     // {
    //     //   from: './src/images',
    //     //   to: 'images'
    //     // }
    //   ]
    // })
  ],

  //引入模块的处理规则
  module: {
    rules: [
      {
        test: /\.css$/,//所有文件扩展名是css的使用当前规则来处理
        //use后面的加载器是按照从后向前的顺序来执行
        // use: ['style-loader', 'css-loader']
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  [
                    'postcss-preset-env'
                  ],
                ]
              }
            }
          }
        ]
      },

      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env' //使用这个预设，会根据浏览器来选择插件转化ES5
              ]
            }
          }
        ]
      },

      {
        test: /\.(jpg|png|gif|jpeg)$/,
        //asset/resource 将资源分割为单独的文件，并导出url，就是之前的 file - loader的功能.
        //asset/inline 将资源导出为dataURL（url(data: )）的形式，之前的 url - loader的功能.
        //asset/source 将资源导出为源码（source code）.之前的 raw - loader 功能.
        //asset 自动选择导出为单独文件或者 dataURL形式（默认为8KB）.之前有url - loader设置asset size limit 限制实现
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 1024 * 1024
          }
        },
        generator: {
          filename: 'images/[contenthash][ext]'
        }
      },

      {
        test: /\.(woff|woff2|ttf|eot|otf)$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[contenthash][ext]'
        }
      }
    ]
  },

  //优化选项
  optimization: {
    minimizer: [
      new CssMinimizerPlugin(),
      '...'
    ],
  },

  //开发服务器
  devServer: {
    //设置本地开发服务器的静态路径
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    //本地开发服务器监听在80端口
    port: 80,
    //打开服务器的模块热更新
    hot: true,
    //打开服务器时自动开启浏览器访问默认首页文件
    open: true,
    //只监控js文件及被js文件引用的文件。
    //html的模板文件没有被js文件引用，所以，模板文件的变化不会触发热更新，因此
    //需要在这里设置：需要被额外监控的文件列表
    watchFiles: ['./src/index.html','./src/login.html','./src/home.html','./src/userinfo.html','./src/addnote.html'],
    proxy:{
      '/api':{
        target:'http://127.0.0.1:3000',
        pathRewrite:{
          '^/api':'/api'
        },
        changeOrigin: true
      }
    }
  }

}